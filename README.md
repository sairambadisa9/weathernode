# WeatherNode

Simple web apps using nothing but Node. 

- Illustrates Nodes powerful folder-based capabilities. 
- Illustrates use of module.export. 
- Illustrates use of require. 

44-563 Developing Web Apps and Services

## Requirements

- Install Git version control system
- Install Node.js, an open-source, cross-platform JavaScript run-time environment for executing JavaScript code on the server. 

## Try it out

1. Log in to BitBucket. 

2. Fork this repo from this BitBucket cloud repository into your own cloud repository.

3. Clone it from your cloud repository down to your local machine. 
